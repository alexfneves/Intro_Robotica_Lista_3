clear;

syms alpha t1 t2 t3 l1 l2 l3 l1 l2 l3 real;
x = [1 0 0]';
y = [0 1 0]';
z = [0 0 1]';


h1 = z;
h2 = -x;
h3 = rotx(-alpha)*z;

p01 = l1*z;
p12 = zeros(3,1);
p23 = l2*y;

R01 = rotz(t1);
R12 = rotx(-t2);
hat_h3 = hat(cos(alpha)*z + sin(alpha)*y);
R23 = eye(3) + sin(t3)*hat_h3 + cos(t3)*hat_h3^2;

R02 = R01*R12;
J = [cross(h1, R01*p12 + R02*p23) cross(R01*h2, R02*p23) zeros(3,1); h1 R01*h2 R02*h3];
J = simplify(J)

