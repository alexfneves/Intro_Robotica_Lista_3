clear;

syms d1 t2 t3 L1 L2 real;
x = [1 0 0]';
y = [0 1 0]';
z = [0 0 1]';

%%

T01 = [eye(3) [0 0 d1]'; 0 0 0 1];
T01(3,4) = d1;
T03 = [cos(t2)*cos(t3) -cos(t2)*sin(t3) sin(t2) L1*sin(t2)
       -sin(t3) -cos(t3) 0 0
       sin(t2)*cos(t3) -sin(t2)*sin(t3) -cos(t2) d1-L1*cos(t2)
       0 0 0 1];
T04 = T03 + [zeros(3) L2*[cos(t2)*cos(t3) -sin(t3) sin(t2)*cos(t3)]'; 0 0 0 0];

subs(T04, [d1, t2, t3], [0, pi/2, 0]);

%%
R12 = [0 1 0; 0 0 -1; -1 0 0];
%R12 = roty(pi/2)*rotx(pi/2);
R12 = R12*rotz(t2);
T12 = [R12 [0 0 0]'; 0 0 0 1];
T02 = T01*T12;


%%
h1 = z;
h2 = z;
h3 = z;

R01 = T01(1:3,1:3);
R02 = T02(1:3,1:3);
R03 = T03(1:3,1:3);
R04 = T04(1:3,1:3);

p01 = T01(1:3,4);
p02 = T02(1:3,4);
p03 = T03(1:3,4);
p04 = T04(1:3,4);


%
J = [h1 cross(R02*h2, p04 - p02) cross(R03*h3, p04 - p03); zeros(3,1) R02*h2 R03*h3];
J = simplify(J)