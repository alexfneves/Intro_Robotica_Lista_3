clear;

syms alpha t1 t2 t3 l1 l2 l3 l1 l2 l3 real;
x = [1 0 0]';
y = [0 1 0]';
z = [0 0 1]';


h1 = z;
h2 = -x;
h3 = y;

p01 = l1*z;
p12 = zeros(3,1);
p23 = l2*y;
p3e = t3*y;

R01 = rotz(t1);
R12 = rotx(-t2);
R23 = eye(3);
R3e = eye(3);

R02 = R01*R12;
R03 = R01*R12*R23;
J = [cross(h1, R01*p12 + R02*p23 + R03*p3e) cross(R01*h2, R02*p23 + R03*p3e) R02*h3; h1 R01*h2 zeros(3,1)];
J = simplify(J)
